
resource "aws_wafregional_ipset" "waf_ips" {
  name = "${var.waf_ipset_name}"

  dynamic "ip_set_descriptor" {
    for_each = var.ip_set_descriptors
    content {
      type  = "IPV4"
      value = ip_set_descriptor.value
    }
  }
}
resource "aws_wafregional_rule" "waf_rule" {
  name        = "${var.waf_rule_name}"
  metric_name = "${var.waf_rule_name}"

  predicate {
    type    = "${var.waf_type_object}"
    data_id = "${aws_wafregional_ipset.waf_ips.id}"
    negated = "${var.negated}"

  }

}

resource "aws_wafregional_rate_based_rule" "waf_rule" {
  depends_on  = ["aws_wafregional_ipset.waf_ips"]
  name        = "${var.waf_rate_name}"
  metric_name = "${var.waf_rate_name}"

  rate_key   = "IP"
  rate_limit = "${var.rate_limit}"

  predicate {
    data_id = "${aws_wafregional_ipset.waf_ips.id}"
    negated = "${var.negated}"
    type    = "${var.waf_type_object}"
  }
}


resource "aws_wafregional_web_acl" "waf_acl" {
  name        = "${var.waf_web_acl_name}"
  metric_name = "${var.waf_web_acl_name}"

  default_action {
    type = "BLOCK"
  }

  rule {
    action {
      type = "ALLOW"
    }

    priority = 1
    rule_id  = "${aws_wafregional_rule.waf_rule.id}"
    type     = "REGULAR"

  }

  rule {
    action {
      type = "ALLOW"
    }

    priority = 2
    rule_id  = "${aws_wafregional_rate_based_rule.waf_rule.id}"
    type     = "RATE_BASED"
  }

}

resource "aws_wafregional_web_acl_association" "waf_alb" {
  resource_arn = "${var.aws_alb_arn}"
  web_acl_id   = "${aws_wafregional_web_acl.waf_acl.id}"

}
