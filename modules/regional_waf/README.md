<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Regional WAF
![WAF](https://gitlab.com/gabrielrojasnyc/gabe_internal_test/raw/master/modules/regional_waf/images/waf-security-automations-architecture.b2488c49739d7522d3372d47c7b36cbaf614d6b9.png?inline=false)

[WAF Official Documentation](https://docs.aws.amazon.com/waf/latest/developerguide/what-is-aws-waf.html)

Creates a WAF and associates it with an Application Load Balancer (ALB)

Creates the following resources:

* Web Application Firewall (WAF)
* Creates rule for WAF to block requests by source IP Address
* Attaches WAF to existing Application Load Balancer (ALB)

## Usage

```hcl
module "regional_waf" {
  source             = "../modules/regional_waf/"
  waf_ipset_name     = local.waf_ipset_name
  waf_rule_name      = local.waf_rule_name
  waf_web_acl_name   = local.waf_web_acl_name
  waf_rate_name      = local.waf_rate_name
  ip_set_descriptors = local.ip_set_descriptors
  waf_type_object    = local.waf_type_object
  negated            = local.negated
  aws_alb_arn        = local.aws_alb_arn
  rate_limit         = local.rate_limit

}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| alb\_arn | ARN of the Application Load Balancer (ALB) to be associated with the Web Application Firewall (WAF) Access Control List (ACL). | string | n/a | yes |
| associate\_alb | Whether to associate an Application Load Balancer (ALB) with an Web Application Firewall (WAF) Access Control List (ACL). | string | `"false"` | yes |
| waf\_ipset\_name | The name or description of the IPSet. | list | `"false"` | yes |
| waf\_rule\_name | The name or description of the rule. | string | `"false"` | yes |
| waf\_web\_acl\_name | The name or description of the web ACL. | string | `"false"` | yes |
| rate\_limit |  The maximum number of requests, which have an identical value in the field specified by the RateKey, allowed in a five-minute period. | number | `"false"` | yes |
 ip\_set\_descriptors | One or more pairs specifying the IP address type (IPV4) and the IP address range (in CIDR notation) from which web requests originate. | string | `"false"` | yes |
| negated| Whether to use the settings or the negated settings that you specified in the objects.| string | `"false"` | yes |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->