variable "waf_ipset_name" {
  description = "WAF IPset name"
  type        = string
}

variable "waf_rule_name" {
  description = "WAF rule name"
  type        = string
}

variable "waf_web_acl_name" {
  description = "WAF web ACL rule"
  type        = string

}

variable "waf_rate_name" {
  description = "Rate based rule name"
  type        = string

}

variable "rate_limit" {
  description = "The maximum number of requests, which have an identical value in the field specified by the RateKey, allowed in a five-minute period."
  type        = number

}

variable "aws_alb_arn" {
  description = "ARN name of the Application Load Balancer"
  type        = string

}

variable "ip_set_descriptors" {
  description = "Specifying the IP address type IPV4 and the IP address range (in CIDR notation) from which web requests originate"
  type        = list(string)
}

variable "waf_type_object" {
  description = "Contains one or more IP addresses or blocks of IP addresses specified in Classless Inter-Domain Routing (CIDR) notation."
  type        = string
}

variable "negated" {
  description = "(Required) Whether to use the settings or the negated settings that you specified in the objects."
  type        = bool
  default     = false
}
