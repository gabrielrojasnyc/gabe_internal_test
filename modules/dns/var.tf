variable "internal_dns_zone" {
  description = "Internal DNS name to be used with in the AWS environment"
  type        = string
}

variable "vpc_id" {
  description = "DNS internal record where VPC should live on"
  type        = string
}

variable "vz_accounts_to_authorize" {
  description = "VPC accounts that need to be authorized in Vz to use internal route 53"
  type        = list(string)

}
