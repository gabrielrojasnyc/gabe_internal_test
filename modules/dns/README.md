# Route 53 DNS
![Internal Private Hosted Zone](https://gitlab.com/gabrielrojasnyc/gabe_internal_test/raw/master/modules/dns/images/v2-fig-04.jpg?inline=false)

![Route 53](https://gitlab.com/gabrielrojasnyc/gabe_internal_test/raw/master/modules/redis_elastic_cache/images/product-pagehttps://docs.aws.amazon.com/Route53/latest/DeveloperGuide/Welcome.html)

Terraform module for deploying and managing [Internal Private Hosted Zone](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/hosted-zones-private.html).

This module supports create privated hosted zones to respond from queries from internal and cross-account VPCs

NOTE: **Currently you need to add permission to cross-account VPCs to use the domain to resolve name** [Cross-account DNS resolution](https://aws.amazon.com/blogs/security/simplify-dns-management-in-a-multiaccount-environment-with-route-53-resolver/)

There are two optiions you need for the creation of the AWS resoruce
- Internal Domain Name
- A list of VPCs from others AWS accounts that will need access to resolve DNS names from this zone

This will result in a internal domain zone that will be only resolve names within AWS accounts


## Usage

Create Elasticache Redis cluster

```hcl
locals {
  dns_zone                 = "aws.gabe.internal"
  vz_accounts_to_authorize = ["vpc-00620d96bd9f18f65"]

}

data "aws_vpc" "default" {}

module "route_53" {
  source                   = "../modules/dns/"
  vpc_id                   = data.aws_vpc.default.id
  internal_dns_zone        = local.dns_zone
  vz_accounts_to_authorize = local.vz_accounts_to_authorize
}
```

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Authors

Originally created by [Contino](https://contino.io),
Module managed by [Verizon]