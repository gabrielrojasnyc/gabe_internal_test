data "aws_region" "current" {}

resource "aws_route53_zone" "internal_zone" {
  name = var.internal_dns_zone
  vpc {
    vpc_id = var.vpc_id
  }
  tags = {
    Environment = "Services"
  }
}


resource "null_resource" "create_vz_accounts_to_authorize" {
  count = "${length(var.vz_accounts_to_authorize)}"

  triggers = {
    zone_id = "${aws_route53_zone.internal_zone.zone_id}"
  }

  provisioner "local-exec" {
    command = "aws route53 create-vpc-association-authorization --hosted-zone-id ${aws_route53_zone.internal_zone.zone_id} --vpc VPCRegion=${data.aws_region.current.name},VPCId=${element(var.vz_accounts_to_authorize, count.index)}"
  }
}
