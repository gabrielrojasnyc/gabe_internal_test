output "Zone_id" {
  description = "Output the Hosted ID zone"
  value       = aws_route53_zone.internal_zone.zone_id
}

output "name_servers" {
  description = "Exported name servers"
  value       = aws_route53_zone.internal_zone.name_servers
}
