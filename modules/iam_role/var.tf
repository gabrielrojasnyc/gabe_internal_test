variable "iam_role_name" {
  description = "The name of the role. If omitted, Terraform will assign a random, unique name."
  type        = string
}

variable "iam_role_description" {
  description = "Description for the role."
  type        = string
}

variable "iam_role_path" {
  description = "The path to the role."
  type        = string
  default     = "/"
}

variable "assume_role_path" {
  description = "Path of the assume policy file"
  type        = string
}

variable "policy_name" {
  description = "The name of the IAM policy."
  type        = string
}

variable "policy_description" {
  description = "Description of the policy"
  type        = string
}

variable "policy_path" {
  description = "The path to the policy."
  type        = string

}

variable "attachment_name" {
  description = "Policy attachment name"
  type        = string

}

