resource "aws_iam_role" "iam_role" {
  name               = var.iam_role_name
  description        = var.iam_role_description
  assume_role_policy = file(var.assume_role_path)
}

resource "aws_iam_policy" "policy" {
  name        = var.policy_name
  description = var.policy_description
  policy      = file(var.policy_path)
}

resource "aws_iam_policy_attachment" "policy-attachment" {
  name       = var.attachment_name
  roles      = [aws_iam_role.iam_role.name]
  policy_arn = aws_iam_policy.policy.arn
}
