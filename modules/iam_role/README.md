<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# IAM Role
![IAM_ROLE](https://gitlab.com/gabrielrojasnyc/gabe_internal_test/raw/master/modules/iam_role/images/iam.png?inline=false)

[IAM Official Documentation](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles.html)

Before an IAM user, application, or service can use a role that you created, you must grant permissions to switch to the role. You can use any policy attached to one of an IAM user's groups or to the user itself to grant the necessary permissions. This section describes how to grant users permission to use a role, and then how the user can switch to a role using the AWS Management Console, the Tools for Windows PowerShell, the AWS Command Line Interface (AWS CLI) and the AssumeRole API.

Important

If you create a role programmatically instead of in the IAM console, then you have an option to add a Path of up to 512 characters in addition to the RoleName, which can be up to 64 characters long. However, if you intend to use a role with the Switch Role feature in the AWS console, then the combined Path and RoleName cannot exceed 64 characters.

You can switch roles from the AWS Management Console. You can assume a role by calling an AWS CLI or API operation or by using a custom URL. The method that you use determines who can assume the role and how long the role session can last.


Create IAM role and associate it with a policy and a role


Creates the following resources:

* IAM role
* IAM policy
* Attaches policy to the role

## Usage

```hcl
module "iam_role" {
  source               = "../modules/iam_role/"
  iam_role_name        = local.iam_role_name
  policy_name          = local.policy_name
  attachment_name      = local.attachment_name
  iam_role_description = local.iam_role_description
  policy_description   = local.policy_description
  assume_role_path     = local.assume_role_path
  policy_path          = local.policy_path

}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| iam\_role\_name | Name of the IAM role. | string | n/a | yes |
| policy\_name | Name of the policy | `"false"` | yes |
| attachment\_name | Name of the policy attachment | list | `"false"` | yes |
| iam\_role\_description | Brief description of the policy | string | `"false"` | yes |
| policy\_description | Description of what the policy does | string | `"false"` | yes |
| assume\_role\_path | The trust policy that is associated with this role. Trust policies define which entities can assume the role. You can associate only one trust policy with a role. | number | `"false"` | yes |
 policy\_path | Adds or updates an inline policy document that is embedded in the specified IAM role. | string | `"false"` | yes |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->