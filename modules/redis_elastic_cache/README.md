# redis_elastic_cache

![Redis Elasticache Redis cluster](https://gitlab.com/gabrielrojasnyc/gabe_internal_test/raw/master/modules/redis_elastic_cache/images/product-page-diagram_ElastiCache_redis_chat-messaging%20.237242d3fff74477a287adc66d3a6bd920c26e31.png?inline=false)

Terraform module for deploying and managing [Amazon Elasticache Service](https://aws.amazon.com/elasticache/redis/?nc=sn&loc=2&dn=1).

This module supports two modes single and multinode Redis cluster

Single-node Amazon ElastiCache Redis clusters are in-memory entities with limited data protection services (AOF). If your cluster fails for any reason, you lose all the cluster's data. However, if you're running the Redis engine, you can group 2 to 6 nodes into a cluster with replicas where 1 to 5 read-only nodes contain replicate data of the group's single read/write primary node. In this scenario, if one node fails for any reason, you do not lose all your data since it is replicated in one or more other nodes. Due to replication latency, some data may be lost if it is the primary read/write node that fails.


NOTE: **Currently this module only supports default encryption and not KMS** At the moment AWS has not released the aws-cli functionality to set up the KMS encryption at creation time [Support for KMS and CMS](https://aws.amazon.com/about-aws/whats-new/2019/08/amazon-elasticache-for-redis-adds-support-for-customer-managed-keys-in-aws-key-management-service-for-encryption-at-rest/)

Several options affect the resilience and scalability of your Elasticache deployment.  For a production deployment:
- "default.redis3.2.cluster.on" for the parameter group name to enable clustering on
- set `instance_count` to at least `2` 
- choose an `instance_type` that is not in the T2 family
- set `automatic_failover_enabled` to `true`.

This will result in a cluster with two (2) dedicated cluster, balanced across two availability zones.

## Usage

Create Elasticache Redis cluster

```hcl
locals {
  dns_zone                 = "aws.gabe.internal"
  cluster_id               = "test-gabe"
  node_type                = "cache.m4.large"
  parameter_group_name     = "default.redis5.0"
  num_cache_clusters       = 2
  vz_accounts_to_authorize = ["vpc-00620d96bd9f18f65"]
  description              = "This is a test cluster"
  maintenance_window       = "sun:05:00-sun:09:00"
  snapshot_window          = "02:00-03:00"

}

module "redis_elastic_cache" {
   source               = "../modules/redis_elastic_cache/"
  cluster_id           = local.cluster_id
  node_type            = local.node_type
  num_cache_clusters   = local.num_cache_clusters
  parameter_group_name = local.parameter_group_name
  description          = local.description
  maintenance_window   = local.maintenance_window
  snapshot_window      = local.snapshot_window
}
```

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Authors

Originally created by [Contino](https://contino.io), [
Module managed by [Verizon]

