variable "cluster_id" {
  description = "Group identifier. ElastiCache converts this name to lowercase"
  type        = string
}

variable "node_type" {
  description = " The compute and memory capacity of the nodes"
  type        = string
}

variable "num_cache_clusters" {
  description = "The initial number of cache nodes that the cache cluster will have"
  type        = number
}
variable "description" {
  description = "Function that the cluster is prroving"
  type        = string
}

variable "parameter_group_name" {
  description = "default parameter group appropriate to your engine version will be used"
  type        = string

}

variable "maintenance_window" {
  description = "specifies the weekly time range for when maintenance on the cache cluster is performed"
  type        = string

}

variable "snapshot_window" {
  description = "The daily time range (in UTC) during which ElastiCache will begin taking a daily snapshot of your cache cluster"
  type        = string

}
