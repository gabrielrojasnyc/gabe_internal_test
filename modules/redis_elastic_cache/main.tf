locals {
  engine         = "redis"
  engine_version = "5.0.4"
  port           = 6379
}
data "aws_secretsmanager_secret" "elasticache-by-arn" {
  arn = "arn:aws:secretsmanager:us-east-1:207117134565:secret:elasticache-eAwQTv"
}

data "aws_secretsmanager_secret_version" "redis-cluster" {
  secret_id = "${data.aws_secretsmanager_secret.elasticache-by-arn.id}"
}


resource "aws_elasticache_subnet_group" "redis_cluster" {
  name       = "${var.cluster_id}-cache-subnet"
  subnet_ids = ["subnet-d0a6fcb7", "subnet-367a5a18"]
}
resource "aws_elasticache_replication_group" "redis_cluster" {
  depends_on                    = ["aws_elasticache_subnet_group.redis_cluster"]
  engine                        = local.engine
  automatic_failover_enabled    = true
  availability_zones            = ["us-east-1a", "us-east-1b"]
  parameter_group_name          = "${var.parameter_group_name}"
  replication_group_id          = "${var.cluster_id}"
  replication_group_description = "${var.description}"
  node_type                     = "${var.node_type}"
  number_cache_clusters         = "${var.num_cache_clusters}"
  engine_version                = local.engine_version
  port                          = local.port
  maintenance_window            = "${var.maintenance_window}"
  snapshot_window               = "${var.snapshot_window}"
  auth_token                    = jsondecode(data.aws_secretsmanager_secret_version.redis-cluster.secret_string)["secret"]
  subnet_group_name             = "${aws_elasticache_subnet_group.redis_cluster.id}"
  at_rest_encryption_enabled    = true
  transit_encryption_enabled    = true
}
