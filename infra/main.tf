provider "aws" {
  region = "us-east-1"
}

locals {
  #DNS module variables
  dns_zone = "aws.gabe.internal"

  # Redis module variables
  cluster_id               = "test-gabe"
  node_type                = "cache.m4.large"
  parameter_group_name     = "default.redis5.0"
  num_cache_clusters       = 2
  vz_accounts_to_authorize = ["vpc-00620d96bd9f18f65"]
  description              = "This is a test cluster"
  maintenance_window       = "sun:05:00-sun:09:00"
  snapshot_window          = "02:00-03:00"

  #WAF module variables
  aws_alb_arn        = "arn:aws:elasticloadbalancing:us-east-1:207117134565:loadbalancer/app/gabe-alb-test/050efbcd0f1707c2"
  waf_ipset_name     = "gabe_test"
  waf_rule_name      = "gabewafrule"
  waf_web_acl_name   = "gabewebacl"
  waf_rate_name      = "gaberaterule"
  ip_set_descriptors = ["10.16.0.0/16", "192.0.7.0/24"]
  rate_limit         = 2000
  waf_type_object    = "IPMatch"
  negated            = false

  #IAM module variable
  iam_role_name        = "gabe-test-role"
  policy_name          = "gabe-test-policy"
  attachment_name      = "gabe-policy-attachment"
  policy_description   = "This is a test policy"
  iam_role_description = "This is test shell role"
  assume_role_path     = "test-role-policy.json"
  policy_path          = "test-policy.json"
}



data "aws_vpc" "default" {}

module "iam_role" {
  source               = "../modules/iam_role/"
  iam_role_name        = local.iam_role_name
  policy_name          = local.policy_name
  attachment_name      = local.attachment_name
  iam_role_description = local.iam_role_description
  policy_description   = local.policy_description
  assume_role_path     = local.assume_role_path
  policy_path          = local.policy_path

}

module "route_53" {
  source                   = "../modules/dns/"
  vpc_id                   = data.aws_vpc.default.id
  internal_dns_zone        = local.dns_zone
  vz_accounts_to_authorize = local.vz_accounts_to_authorize
}

module "redis_elastic_cache" {
  source               = "../modules/redis_elastic_cache/"
  cluster_id           = local.cluster_id
  node_type            = local.node_type
  num_cache_clusters   = local.num_cache_clusters
  parameter_group_name = local.parameter_group_name
  description          = local.description
  maintenance_window   = local.maintenance_window
  snapshot_window      = local.snapshot_window

}

module "regional_waf" {
  source             = "../modules/regional_waf/"
  waf_ipset_name     = local.waf_ipset_name
  waf_rule_name      = local.waf_rule_name
  waf_web_acl_name   = local.waf_web_acl_name
  waf_rate_name      = local.waf_rate_name
  ip_set_descriptors = local.ip_set_descriptors
  waf_type_object    = local.waf_type_object
  negated            = local.negated
  aws_alb_arn        = local.aws_alb_arn
  rate_limit         = local.rate_limit

}


